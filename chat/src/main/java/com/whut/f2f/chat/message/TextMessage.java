package com.whut.f2f.chat.message;

/**
 * 文本消息
 *
 * Created by null on 2017/1/20.
 */
public class TextMessage extends Message {

    private MessageType messageType = MessageType.TEXT;

    private Scope scope = Scope.SEND;

    private String content;

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
