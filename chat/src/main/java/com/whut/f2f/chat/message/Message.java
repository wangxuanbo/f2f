package com.whut.f2f.chat.message;

import java.util.Date;

/**
 * 消息
 *
 * Created by null on 2017/1/20.
 */
public abstract class Message {

    private String from;
    private String to;
    private Date createTime;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
