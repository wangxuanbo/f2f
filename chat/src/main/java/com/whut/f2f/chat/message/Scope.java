package com.whut.f2f.chat.message;

/**
 * 消息周期
 * ping，send，disconnect
 *
 * Created by null on 2017/1/25.
 */
public enum Scope {
    PING, SEND
}
