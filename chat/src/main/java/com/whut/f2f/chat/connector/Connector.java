package com.whut.f2f.chat.connector;

import io.netty.channel.Channel;

/**
 * Channel连接
 *
 * Created by null on 2017/1/25.
 */
public interface Connector {

    void add(String id, Channel channel);

    void remove(String id);

    void send(String id, String msg);

    void sendAll(String msg);
}
