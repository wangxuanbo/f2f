package com.whut.f2f.chat;

/**
 * 启动类
 *
 * Created by null on 2017/1/20.
 */
public class Starter {

    public static void main(String[] args) {
        new ChatServer().bind(8090);
    }

}
