package com.whut.f2f.chat.message;

/**
 * ping，服务端是否可连接
 *
 * Created by null on 2017/1/25.
 */
public class PingMessage {

    private String from;

    private Scope scope = Scope.PING;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }
}
