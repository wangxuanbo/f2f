package com.whut.f2f.chat.message;

import com.whut.f2f.common.util.StringUtils;

import java.util.HashMap;

/**
 * 消息格式校验
 *
 * Created by null on 2017/2/9.
 */
public class MessageValidation {

    protected static boolean sendMessageValid(HashMap data) {
        String from = (String) data.get("from");
        String to = (String) data.get("to");
        String createTime = (String) data.get("createTime");
        String messageType = (String) data.get("messageType");
        String content = (String) data.get("content");
        return !StringUtils.isEmpty(from) && !StringUtils.isEmpty(to) && !StringUtils.isEmpty(createTime) &&
                !StringUtils.isEmpty(messageType) && !StringUtils.isEmpty(content);
    }

}
