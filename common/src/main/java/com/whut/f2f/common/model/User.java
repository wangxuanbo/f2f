package com.whut.f2f.common.model;

import java.util.List;

/**
 * 用户
 *
 * Created by null on 2017/2/9.
 */
public class User {
    private String userId;
    private List<Account> accounts;
    private boolean online;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }
}
