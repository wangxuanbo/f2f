package com.whut.f2f.common.util;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * json转化器
 *
 * Created by null on 2017/1/25.
 */
public class Json {

    private static final Logger log = LoggerFactory.getLogger(Json.class);

    private static final Gson gson = new Gson();

    public static <T> T parse(Class<T> clazz, String json) {
        try {
            return gson.fromJson(json, clazz);
        } catch (JsonSyntaxException e) {
            log.warn("json解析失败");
            return null;
        }
    }

    public static String parse(Object obj) {
        return gson.toJson(obj);
    }
}
