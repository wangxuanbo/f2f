package com.whut.f2f.common.util;

/**
 * Created by null on 2017/2/9.
 */
public class StringUtils {

    public static boolean isNull(String value) {
        return value == null;
    }

    public static boolean isEmpty(String value) {
        return isNull(value) || value.isEmpty();
    }

}
