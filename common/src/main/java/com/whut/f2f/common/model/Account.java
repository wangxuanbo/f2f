package com.whut.f2f.common.model;

/**
 * 账号
 *
 * Created by null on 2017/2/9.
 */
public class Account {

    private String accountId;
    private String username;
    private String password;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
