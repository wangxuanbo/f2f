## 1.基于netty的及时聊天消息设计

ping消息，如果服务端正常，将会将该channel纳入管理
```
{
	"scope": "ping",
	"from": "userId"
}$_$
```

send消息
```
{
	"scope": "send",
	"from": "userId",
	"to": "otherId",
	"createTime": "yy:MM:dd hh:mm:ss"
	"messageType": "text", // 文本消息
	"content": "message" // 消息内容
}$_$
```

消息格式不正确，服务端返回
```
{
	"code": 412,
	"msg": "格式不正确!"
}
```